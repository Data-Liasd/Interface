// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#include "bdd_win.h"

BDDConnectionDialog::BDDConnectionDialog(QWidget *parent)
    : QDialog(parent)
{
  qDebug() << "BDDConnectionDialog(): " << "Initialisation de la connexion et de la fenêtre";
  //name of settings file
  SettingsFile = "bd.ini" ;
  //name of ressources files
  InitDBFile = ":/resources/initDB.sql" ;
  ProjName= readFromFile(":/resources/projName.txt");
  SettingsPath = QDir::homePath() + "/." + ProjName ;
  Bdd = new BDD();
  // this dialog is modal
  setModal(true);
  // the title of this dialog
  setWindowTitle(tr("Database connection")) ;
  // place each GUI component
  setUpGUI();
  // load available drivers
  findAvailableDrivers();
  // set the data from bd.ini or default values
  //that will be presented to the user as auto-filled form
  loadSettings() ;

  connect(this,SIGNAL(rejected()),this,SLOT(slotQuit()));
}

BDDConnectionDialog::~BDDConnectionDialog()
{
    qDebug() << "~BDDConnectionDialog(): " << "supprimer BDD";
    if (Bdd!=NULL) Bdd->~BDD();
}

void BDDConnectionDialog::setUpGUI()
{
  // Global layout
  QVBoxLayout * verticallayout = new QVBoxLayout(this) ;
  verticallayout->addStretch() ;
  //verticallayout->setSizeConstraint(QLayout::SetNoConstraint);
  
  // High part of the window
  QGridLayout * highlayout = new QGridLayout();
  QGroupBox * highbox = new QGroupBox(this) ;
  highbox->setTitle(tr("Database connection properties")) ;
  
  QLabel * namelabel = new QLabel(tr("Database Type (driver name)")) ;
  highlayout->addWidget(namelabel, 0, 0 );
  DatabaseDriverNameCombo = new QComboBox() ;
  DatabaseDriverNameCombo->setEditable(false) ;
  DatabaseDriverNameCombo->setFocus() ;
  highlayout->addWidget(DatabaseDriverNameCombo, 0, 1 );
  namelabel->setBuddy(DatabaseDriverNameCombo);

  QLabel * hostnamelabel = new QLabel(tr("Host Name")) ;
  DatabaseHostName = new QLineEdit() ;
  highlayout->addWidget(hostnamelabel, 1, 0 );
  highlayout->addWidget(DatabaseHostName, 1, 1);
  hostnamelabel->setBuddy(DatabaseHostName);
  connect(DatabaseHostName, SIGNAL (editingFinished()), this, SLOT (slotCheckFormData())) ;

  QLabel * portlabel = new QLabel(tr("TCP/IP Port Number")) ;
  DatabasePort = new QSpinBox() ;
  DatabasePort->setMaximum(9999) ;
  DatabasePort->setMinimum(100) ;
  DatabasePort->setSingleStep(1) ;
  highlayout->addWidget(portlabel, 2, 0 );
  highlayout->addWidget(DatabasePort, 2, 1 );
  portlabel->setBuddy(DatabasePort) ;
  
  QLabel * dbnamelabel = new QLabel(tr("Database Name")) ;
  DatabaseName = new QLineEdit();
  highlayout->addWidget(dbnamelabel, 3, 0 );
  highlayout->addWidget(DatabaseName, 3, 1 );
  dbnamelabel->setBuddy(DatabaseName);
  connect(DatabaseName, SIGNAL (editingFinished()), this, SLOT (slotCheckFormData())) ;
  
  QLabel * usernamelabel = new QLabel(tr("Username")) ;
  DatabaseUsername = new QLineEdit() ;
  highlayout->addWidget(usernamelabel, 4, 0 );
  highlayout->addWidget(DatabaseUsername, 4, 1 );
  usernamelabel->setBuddy(DatabaseUsername);
  connect(DatabaseUsername, SIGNAL (editingFinished()), this, SLOT (slotCheckFormData())) ;
  
  QLabel * passwordlabel = new QLabel(tr("Password")) ;
  DatabasePassword = new QLineEdit() ;
  DatabasePassword->setEchoMode(QLineEdit::Password) ;
  highlayout->addWidget(passwordlabel, 5, 0 );
  highlayout->addWidget(DatabasePassword, 5, 1 );
  passwordlabel->setBuddy(DatabasePassword);
  connect(DatabasePassword, SIGNAL (editingFinished()), this, SLOT (slotCheckFormData())) ;
  connect(DatabasePassword, SIGNAL (returnPressed()),   this, SLOT (slotCheckFormData())) ;

  // complete highbox
  highbox->setLayout(highlayout) ;
  verticallayout->addWidget(highbox) ;

  // Low part 

  // Settings
  
  QGroupBox * settingsbox = new QGroupBox() ;
  settingsbox->setTitle(tr("Configuration files")) ;
  QGridLayout * settingslayout = new QGridLayout() ;
  
  QLabel * pathlabel = new QLabel(tr("Path of settings file (editable name)")) ;
  settingslayout->addWidget(pathlabel, 0, 0) ;
  QLabel * settingspath = new QLabel(SettingsPath + "/") ;
  settingslayout->addWidget(settingspath, 0, 1) ;
  QLineEdit * file = new QLineEdit(SettingsFile) ;
  connect(file, &QLineEdit::textChanged, [=](QString text) {SettingsFile = text ;}) ;
  settingslayout->addWidget(file, 0, 2) ;
  
  SaveButton = new QPushButton(tr("Save DB Settings"));
  settingslayout->addWidget(SaveButton, 1, 1);
  SaveButton->setEnabled(false) ;
  connect(SaveButton, SIGNAL (clicked()), this, SLOT (slotSaveSettings()));
  
  QLabel * initdbfile = new QLabel(tr("For developpers: path of DB initializer file")) ;
  settingslayout->addWidget(initdbfile, 2, 0);
  QLineEdit * initdbfileurl = new QLineEdit(InitDBFile) ;
  initdbfileurl->setDisabled(true);
  settingslayout->addWidget(initdbfileurl, 2, 1) ;
 
  settingsbox->setLayout(settingslayout) ;
  verticallayout->addWidget(settingsbox);

  // database connection URL part
    
  QGroupBox * url = new QGroupBox() ;
  url->setTitle(tr("Database URL")) ;
  QHBoxLayout * urllayout = new QHBoxLayout() ;
  DatabaseURLLabel = new QLabel() ;
  DatabaseURLLabel->setAlignment(Qt::AlignCenter) ;
  urllayout->addWidget(DatabaseURLLabel) ;
  
  url->setLayout(urllayout) ;
  verticallayout->addWidget(url) ;
  
  // Buttons part
  
  QGroupBox * btngrp = new QGroupBox() ;
  QHBoxLayout * btnlayout = new QHBoxLayout();
  btngrp->setLayout(btnlayout);
 
  ConnectButton = new QPushButton(tr("Connect"));
  ConnectButton->setEnabled(false) ;
  btnlayout->addWidget(ConnectButton);
  connect(ConnectButton, SIGNAL (clicked()), this, SLOT (slotPerformConnection())) ;

  QPushButton * initdbbtn = new QPushButton(tr("Init DB")) ;
  connect(initdbbtn, SIGNAL (clicked()), this, SLOT (slotInitDB())) ;
  btnlayout->addWidget(initdbbtn);

  LoadDBButton = new QPushButton(tr("Load Settings")) ;
  connect(LoadDBButton,SIGNAL(clicked()), this , SLOT(slotLoadSettings()));
  btnlayout->addWidget(LoadDBButton);

  verticallayout->addWidget(btngrp) ;
  
  //resizeToContent() ;
}

QString BDDConnectionDialog::makeSettingsFileUrl()
{
  qDebug() << "Settings file url: " << SettingsPath + "/" + SettingsFile ;
  return SettingsPath + "/" + SettingsFile ;
}

// ================================================ High part functions and slots


void BDDConnectionDialog::findAvailableDrivers()
{
  // remove all items
  DatabaseDriverNameCombo->clear();
  // populate the combo box with all available drivers
  foreach(QString driverName, QSqlDatabase::drivers()) DatabaseDriverNameCombo->addItem(driverName) ;
}

// inutile ?
void BDDConnectionDialog::run()
{
    checkFormData();
    this->exec();
}

bool BDDConnectionDialog::slotCheckFormData() {return checkFormData();}

bool BDDConnectionDialog::checkFormData()
{
  if (DatabaseName->text().isEmpty()
      or DatabaseHostName->text().isEmpty()
      or DatabaseUsername->text().isEmpty()
      or DatabasePassword->text().isEmpty() )
  {
    ConnectButton->setEnabled(false);
    SaveButton->setEnabled(false);
  }
  else
  {
    // enable the connect button and give focus
    ConnectButton->setEnabled(true) ;
    SaveButton->setEnabled(true) ;
    ConnectButton->setFocus() ;
  }

  // display the URL if the form is completed
  if (ConnectButton->isEnabled())
    DatabaseURLLabel->setText
    (
      DatabaseDriverNameCombo->currentText()
      + "://"
      + DatabaseUsername->text()
      + "@"
      + DatabaseHostName->text()
      + "/"
      + DatabaseName->text()
    ) ;
  else DatabaseURLLabel->setText("") ;

  return ConnectButton->isEnabled() ;
}

//========================================= Low part slots

void BDDConnectionDialog::slotSaveSettings()
{
  qDebug() << "slotSaveSettings(): " << "saving settings" ;
  QString serveur = DatabaseHostName->text() ;
  QString user = DatabaseUsername->text() ;
  QString pwd = DatabasePassword->text() ;
  QString db = DatabaseName->text() ;
  int port = DatabasePort->value() ;
  QString drv = DatabaseDriverNameCombo->currentText() ;
  QSettings settings (makeSettingsFileUrl(), QSettings::NativeFormat) ;
  settings.setValue("Serveur", serveur) ;
  settings.setValue("BDD", db) ;
  settings.setValue("UserName",user) ;
  settings.setValue("Password",pwd) ;
  settings.setValue("Port",port) ;
  settings.setValue("Driver",drv) ;
  QMessageBox::information(this, tr("Settings saved"), tr("Settings saved in file ") + makeSettingsFileUrl());
}

void BDDConnectionDialog::slotPerformConnection()
{
  // perform anotherslotPerformConnection check against the user data
  if (checkFormData()) doDatabaseConnection() ;
}

void BDDConnectionDialog::doDatabaseConnection()
{
  if (! QSqlDatabase::isDriverAvailable(DatabaseDriverNameCombo->currentText()))
  {
    qDebug() << "doDatabaseConnection(): " << "Database driver not available!" ;
    QMessageBox::critical(this, tr("Database Connection Error"), tr("Database driver not available")) ;
    return ;
  }
  Bdd->setConnexion
    (DatabaseHostName->text(), 
     DatabaseUsername->text(), 
     DatabasePassword->text(), 
     DatabaseName->text(), 
     DatabasePort->value(), 
     DatabaseDriverNameCombo->currentText()
    ) ;
  
  if(! Bdd->getConnexion())
  {
    qDebug() << "doDatabaseConnection(): " << "No connection available" ;
    QMessageBox::critical(this, tr("Database Connection Error"), BDD::DatabaseConnection->lastError().text());
    // disable the connect button and set the focus again on the first field
    ConnectButton->setEnabled(false);
    SaveButton->setEnabled(false);
    DatabaseURLLabel->setText(DatabaseURLLabel->text()+"--->Connexion echouee");
    DatabaseHostName->setFocus();
  }
  else
  {
    qDebug() << "doDatabaseConnection(): " << "OK" ;
    // hide the dialog
    DatabaseURLLabel->setText(DatabaseURLLabel->text()+"--->Connexion etablie");
    qDebug() << "doDatabaseConnection(): " << DatabaseURLLabel->text();
    this->hide();
    // emit the signal
    qDebug() << "doDatabaseConnection(): " << "Emitting signal since the database is connected!" << BDD::DatabaseConnection;
    emit databaseConnect(BDD::DatabaseConnection);
  }
}

void BDDConnectionDialog::slotHandleNewDatabaseConnection(QSqlDatabase * db)
{
  qDebug() << "slotHandleNewDatabaseConnection(): " << "slot new database connection" <<db->connectionName();
  if (BDD::DatabaseConnection->connectionName()!=db->connectionName() && !BDD::DatabaseConnection->isOpen() )
  {
    BDD::DatabaseConnection=db;
    qDebug() << "slotHandleNewDatabaseConnection(): " << "slot new database connection" <<db->connectionName();
  }
}

void BDDConnectionDialog::loadSettings()
{
    QSettings settings(makeSettingsFileUrl(),QSettings::NativeFormat);
    DatabaseHostName->setText(settings.value("Serveur","localhost").toString());
    DatabaseName->setText(settings.value("BDD","CorpusDB").toString());
    DatabaseUsername->setText(settings.value("UserName","charprops").toString());
    DatabasePassword->setText(settings.value("Password","charprops").toString());
    DatabasePort->setValue(settings.value("Port","5432").toInt());
    int idx=DatabaseDriverNameCombo->findText(settings.value("Driver","QPSQL").toString());
    DatabaseDriverNameCombo->setCurrentIndex(idx);
    
}

void BDDConnectionDialog::slotInitDB()
{
  BDD * db = new BDD() ;
  db->setConnexion(DatabaseHostName->text(),DatabaseUsername->text(),DatabasePassword->text(),DatabaseName->text(),DatabasePort->value(),DatabaseDriverNameCombo->currentText()) ;
  if(!db->getConnexion())
  {
    qDebug() << "slotInitDB(): " << "No connection available" ;
    QMessageBox::critical(this, tr("Erreur de Connexion"),"Assurez-vous que l'administrateur vous a créé cette base et vous en a donné les droits" + db->DatabaseConnection->lastError().text());
    DatabaseHostName->setFocus();
  }
  else
  {
    qDebug() << "slotInitDB(): " << "Êtes-vous sûr de vouloir continuer ?" ;
    QMessageBox::StandardButton reply ;
    reply = 
      QMessageBox::question
      (this, 
       "BDD Existe", 
       "Cette procédure vide l'ancienne base de données et recrée ses tables. Êtes-vous sûr de vouloir continuer ?", 
       QMessageBox::Yes|QMessageBox::No
      ) ;
    //si BDD existe et ne veut pas remplacer // set focus on the first field
    if (reply == QMessageBox::No)
    {
      qDebug() << "slotInitDB(): " << "Vous pouvez changer de base de donnée";
      QMessageBox::critical(this, "BDD Existe", tr("Vous pouvez changer de base de données pour une autre déjà existante"));
      DatabaseHostName->setFocus();
    }
    //si BDD existe et choisit de la remplacer
    else
    {
        QMessageBox::StandardButton reply ;
        reply =
          QMessageBox::question
          (this,
           "Dump ancienne BDD",
           "Voulez-vous sauvegarder l'ancienne base de données ?",
           QMessageBox::Yes|QMessageBox::No
          ) ;
        if (reply==QMessageBox::Yes)
        {
            qDebug() << "slotInitDB(): " << "Sauvegarder l'ancienne BDD";
            QString dumpdburl;
            QString path
              =
              QFileDialog::getExistingDirectory
              (this,
               tr("Find dump directory"),
               SettingsPath,
               QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
              ) ;
            if (path.isEmpty())
                dumpdburl=SettingsPath+"/dumpDB.sql";
            else
                dumpdburl=path+"/dumpDB.sql";
            if (db->dumpDB(dumpdburl,DatabaseName->text(),DatabaseHostName->text(),DatabasePort->value(),DatabaseUsername->text(), DatabasePassword->text()))
                QMessageBox::information(this, "Backup BDD", tr("Backup de BDD réalisée avec succès dans le fichier: ")+SettingsPath+"/dumpDB.sql");
            else
                QMessageBox::critical(this, "Backup BDD", tr("Backup de BDD a échouée!"));
        }
      qDebug() << "slotInitDB(): " << "Remplacer BDD";
      int sr=db->replaceDB(InitDBFile);
      // tester le résultat avant de dire que ça a marché
      if (sr==1)
          QMessageBox::information(this, "BDD créée", tr("BDD créée avec succès!"));
      else if(sr==-1)
          QMessageBox::critical(this, "Erreur création BDD", tr("BDD n'est pas créée! Erreur d'ouvrir le fichier sql"));
      else
          QMessageBox::critical(this, "Erreur création BDD", tr("BDD n'est pas completemnt créée! Erreurs dans une ou plusieurs requetes"));
      qDebug() << "slotInitDB(): " << "Fin";
    }
  }
}

void BDDConnectionDialog::slotQuit()
{
    qDebug() << "slotQuit(): " << "Quitter Application";
    QApplication::quit();
}

void BDDConnectionDialog::slotLoadSettings()
{
    loadSettings() ;
    checkFormData() ;

}

QString BDDConnectionDialog::readFromFile(QString filename)
{
    QFile f (filename) ;
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "readFromFile(): " << "cannot open file ; filename set by default to \"Data-liasd\"" ;
        return "Data-liasd";
    }
    QString content = f.readAll().simplified() ;
    qDebug() << "readFromFile(): Content: " << content;
    //test valid filename
    QDir dir(QDir::homePath() + "/." + content);
    if (dir.makeAbsolute()) //valid filename
    {
        qDebug() << " readFromFile(): valid filename" << content;
        return content ;
    }
    qDebug() << " readFromFile(): invalid filename replaced by default \"Data-liasd\"";
    return "Data-liasd" ;
}

// Copyright 2015-2019 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#ifndef BDD_H
#define BDD_H

#include <QtSql>
#include "../Common.h"

typedef QMap<id, double> maLigne ;
typedef QMap<id, maLigne *> maMatrice ;
typedef QPair <QString,int> MyMap;


class BDD : public QObject
{
    Q_OBJECT

private:

    /*!
     * result of database connexion
     */
    bool Connexion ;
public:
      BDD(QString serveur, QString user, QString pwd, QString db, int port, QString _DatabaseDriverName) ;
      BDD(): Connexion(false) {}
      ~BDD() ;
      
      static QSqlDatabase * DatabaseConnection ;
      bool      getConnexion() { return Connexion; }
      void      setConnexion(QString serveur, QString user, QString pwd, QString db, int port,QString _DatabaseDriverName);
      void      fermerConnexion();
      void      ouvrirConnexion();
      static    QString   getdbStatus();
      static    QStringList nameapp;
      int       replaceDB(QString);//pour remplacer bdd retourne 1 si bien remplcaee -1 si fichier non trouve et 0 si quelques/tous les requetes ne sont pas bien executees
      void      restoreDBFromSqlFile(QString, QString , QString , QString );
      bool      dumpDB(QString filename, QString databasename, QString hostname, int port, QString username, QString pass);
};

#endif // BDD_H

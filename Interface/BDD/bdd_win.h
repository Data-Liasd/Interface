// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#ifndef BDD_WIN_H
#define BDD_WIN_H

#define QT5

#include <QtWidgets>
#include <QSettings>
#include "bdd.h"

class BDDConnectionDialog : public QDialog
{
  Q_OBJECT

private:

    QComboBox * DatabaseDriverNameCombo ; // The combo from which the user can select the database driver name and type
    QLineEdit * DatabaseHostName ;        // The editable name of the database name to which connect.
    QSpinBox *  DatabasePort ;      // The database listening port
    QLineEdit * DatabaseName ;        // The editable name of the database to which the user wants to connect
    QLineEdit * DatabaseUsername ;    // The editable username to use for the connection.
    QLineEdit * DatabasePassword ;    // The editable password to use for the connection.

    QPushButton * ConnectButton;   //button to connect to db
    QPushButton * SaveButton;      //button to save default db settings
    QPushButton * LoadDBButton;    //button to load settings
    
    void      setUpGUI() ;               // create all the components of this dialog window and lay them out correctly.
    QString   makeSettingsFileUrl() ;
    void      findAvailableDrivers() ;   // Searches for and populates the combo box with the available database drivers.
    void      doDatabaseConnection() ;   // Performs the connection to the database and emits the signal to pass the connection.
    QString   readFromFile(QString) ;

public:
  
    BDDConnectionDialog(QWidget * parent = 0) ;
    ~BDDConnectionDialog() ;

    bool checkFormData(); // checks user data and enables connect button depending on the form fill status and database connection

     /*!
      * Performs the database connection or prompt the user
      * showing this dialog in the case data is not completed
      * or should not perform the autoconnection.
      * autoConnect if set to true tries to perform an automatic
      * connection to the database, if the data is complete, or prompt the user
      * for missing data. If set to false, simply shows the dialog and waits.
      */
    void run();

/************************/
    void            loadSettings() ;
    BDD *           Bdd ;
    QString         SettingsFile ;
    QString         SettingsPath ;           // the path of settings
    QString         InitDBFile ;
    QString         ProjName;
/********************/

     QLabel * DatabaseURLLabel ; // A label to display the summary database URL connection string.

signals:

      /*!
      * Passes the database connection in the case the connection
      * is succesful.
      */
    void databaseConnect(QSqlDatabase *);

public slots:

    bool slotCheckFormData() ; // public version of checkFormData
    void slotPerformConnection() ; // performs the database connection
    void slotLoadSettings() ;


/********/
      void            slotSaveSettings();
      void            slotInitDB();
      void            slotQuit();
      void            slotHandleNewDatabaseConnection(QSqlDatabase *);
/********/
} ;

#endif // BDD_WIN_H

// progressDialog
//no cursos

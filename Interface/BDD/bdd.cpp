// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#include "bdd.h"

using namespace std ;

QSqlDatabase * BDD::DatabaseConnection ;
QStringList BDD::nameapp({"CorpusChar","CorpusWord","SurveyWord2Vec", "SurveyGlove"}) ;

/************************Connexion a la BDD**************************************************************/

BDD::BDD(QString serveur, QString user, QString pwd , QString db, int port,QString _DatabaseDriverName)
  : Connexion(false)
{
  setConnexion(serveur, user, pwd, db, port, _DatabaseDriverName) ; // Connexion à la base en tant qu'administrateur
}

BDD::~BDD()
{ if (Connexion)
  {
    if (BDD::DatabaseConnection != NULL)
    {
      BDD::DatabaseConnection->close() ;
      QSqlDatabase::removeDatabase(BDD::DatabaseConnection->connectionName()) ;
      delete BDD::DatabaseConnection ;
      Connexion = false ;
      qDebug() << "BDD(): " << "Connexion fermée" << endl ;
    }
  }
}

void BDD::fermerConnexion()
{ if (Connexion)
  {
    BDD::DatabaseConnection->close() ;
    qDebug() << "fermerConnexion(): " << "Connexion fermée" << endl ;
    Connexion = false;
  }
}

void BDD::ouvrirConnexion()
{ if (BDD::DatabaseConnection->open())
  {
    qDebug() << "ouvrirConnexion(): " << "Vous êtes maintenant connecté à " << BDD::DatabaseConnection->databaseName() << "-->" << BDD::DatabaseConnection->hostName() << endl ;
    Connexion = true ;
  }
  else
  {
    qDebug() << "ouvrirConnexion(): " << "La Connexion a échoué, désolé" << endl ;
    Connexion = false ;
  }
}

void BDD::setConnexion (QString serveur, QString user, QString pwd, QString nomBase, int port, QString _DatabaseDriverName)
{
  if (not Connexion)
  {
    qDebug() << "setConnexion(): " << "Performing the database driver setup..";
    // set database driver properties
    QUuid uniqueId = QUuid::createUuid() ;
    QString ConnexionID = uniqueId.toString() ;
    BDD::DatabaseConnection = new QSqlDatabase(QSqlDatabase::addDatabase(_DatabaseDriverName,ConnexionID)) ;
    BDD::DatabaseConnection->setDatabaseName(nomBase);
    BDD::DatabaseConnection->setUserName(user);
    BDD::DatabaseConnection->setHostName(serveur);
    BDD::DatabaseConnection->setPassword(pwd);
    BDD::DatabaseConnection->setPort(port);
    if (! BDD::DatabaseConnection->open())
    {
      qDebug() << "setConnexion(): " << "La Connexion a échoué, désolé : " << BDD::DatabaseConnection->lastError().text() ;
      Connexion=false;
    }
    else
    {
      qDebug() << "setConnexion(): " << "La Connexion a réussi." ;
      Connexion = true ;
    }
  }
  else qDebug() << "setConnexion(): " << "Vous êtes déjà connecté à une base de données " ;
}

QString BDD::getdbStatus()
{
  if (BDD::DatabaseConnection == NULL) return (QString) "Connexion non établie" ;

  return
    BDD::DatabaseConnection->driverName()
      + "://"
      + BDD::DatabaseConnection->userName()
      + "@"
      + BDD::DatabaseConnection->hostName()
      + "/"
      + BDD::DatabaseConnection->databaseName()
      + "--->Connexion "
      +(BDD::DatabaseConnection->isValid() ? (BDD::DatabaseConnection->isOpen() ? "ouverte" : "fermée") : "échouée") ;
}

int BDD::replaceDB(QString filename)
{
  int sr=1;
  qDebug() << "replaceDB(): from filename:" << filename;
  QFile f (filename) ;
  if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
  {
      qDebug() << "replaceDB(): " << "cannot open sql file" ;
      return -1;
  }
  QTextStream in(&f) ;
  QString line ;
  QString sqlStatement = "" ;
  while (! in.atEnd())
  {
    line = in.readLine() ;
    if (line.startsWith('-') || line.isEmpty()) continue ;
    if (line.contains("%1")) line.replace("%1",BDD::DatabaseConnection->userName()) ;
    sqlStatement += line ;
    if(sqlStatement.trimmed().endsWith(";"))
    {
      QSqlQuery query (* BDD::DatabaseConnection) ;
      if (! query.exec(sqlStatement))
      {
          qDebug() << "replaceDB(): " << "query not executed" << sqlStatement;
          sr=0;
      }
      sqlStatement = "" ;
    }
  }
  return sr;
}

void BDD::restoreDBFromSqlFile(QString filename, QString hostname, QString username, QString databasename)
{
    QString out;
    string cmd;
    out= QString("psql -h %1 -U %2 -d %3 -1 -f %4").arg(hostname).arg(username).arg(databasename).arg(filename);
    cmd=out.toStdString();
    system(cmd.c_str());
}

bool BDD::dumpDB(QString filename, QString databasename, QString hostname, int port, QString username, QString pass)
{
    qDebug() << "dumpDB() to fileName:" << filename;
    QProcess process;
    QString str=QString("postgresql://%1:%2@%3:%4/%5").arg(username).arg(pass).arg(hostname).arg(port).arg(databasename);
    process.start(QString("pg_dump --dbname=%1 --file=%2").arg(str).arg(filename));
    process.waitForFinished(-1); // will wait forever until finished

    QString stdout = process.readAllStandardOutput();
    QString stderr = process.readAllStandardError();
    //qDebug() << "dumpDB(): " << "out: " << stdout;
    qDebug() << "dumpDB(): " << "err: " << stderr;
    return stderr.isEmpty();
}

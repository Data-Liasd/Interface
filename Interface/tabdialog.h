#ifndef TABDIALOG_H
#define TABDIALOG_H
#include <QDialog>
#include <QtWidgets>
#include <QMainWindow>
// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license

#include "../Interface/BDD/bdd.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QString = tr("Interface")) ; // nom par défaut

    void           createTab(int _idxtTab, QString _TabTitle, QWidget *_metaobject);
    static QLabel  * createLabel() ;
    static void    msgBox(QString);

    QTabWidget     * Father ; // parent for all the tabs in tabgroup

private slots:
    void            open(); // inutile ?
    void            onTabChanged(int);
    void            closeapp();
   
private:
    void           createActions();
    void           createMenus();
    void           setWStatusBar();

    QMenu          * FileMenu ;
    QAction        * OpenAct ;
} ;

// l'objet contenant des paramètres partagés
class Shared : public QObject
{
  Q_OBJECT
  
  public:
    Shared() ;
    ~Shared() ;
} ;

class Tab ; // forward declaration

// le widget correspondant à l'application (= boxgroup)
class Appli : public QWidget
{
  Q_OBJECT

  public:
    Appli() ;
    ~Appli() ;
    
    Shared * SharedParams ;
    Tab * MyTab ;
    
    void addBox(QString) ; // inutile ?
} ;

class Box : public QGroupBox
{
  Q_OBJECT

  public:

    Box(QString) ;
    ~Box() ;

    Appli * App ;
  
} ;


// l'onglet
class Tab : public QWidget
{
  Q_OBJECT

  public:
  
    Tab() ;
    ~Tab() ;

    Appli * App ;
    QLabel * AnswerZone ;
    
    void initTab(Appli *, Shared * = NULL) ;
    
    void notImplementedMessage(QString) ;
    void answerMessage(QString) ;
  
  private:
    
    void initAppli(Appli *) ;
    void initShared(Shared *, Appli *) ;
    
} ;

struct tabstruct
{
    int tabindex ;
    QString tabtitle ;
    QWidget * tabclass ;
} ;


class TabGroup: public QObject
{
    Q_OBJECT
    
public :
  
   TabGroup() ;
   ~TabGroup();
   void createTabs(MainWindow *) ;
   void addTab(int, QString, Tab *);

   QList<tabstruct> Tabs ;

protected :
  

} ;


#endif // TABDIALOG_H

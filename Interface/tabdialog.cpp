// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#include "tabdialog.h"

//============================================================================ WINDOW ========

MainWindow::MainWindow(QString title)
{
  setWindowTitle(title) ;
  
  createActions() ;
  createMenus() ;
  menuBar()->setHidden(false) ;
  
  Father = new QTabWidget ;
  QWidget * center = new QWidget ;
  setCentralWidget(center) ;
  
  //AnswerZone = new QLabel ;
  //mainlayout->addWidget(AnswerZone, 6, 0, 1, 4) ;
  
  QWidget * bottomfiller = new QWidget ;
  bottomfiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding) ;

  QVBoxLayout * layout = new QVBoxLayout ;
  layout->setMargin(5) ;
  layout->addWidget(Father) ;
  layout->addWidget(bottomfiller) ;
  center->setLayout(layout) ;
  connect(Father, SIGNAL(currentChanged(int)), this, SLOT(onTabChanged(int))) ;
  
  setWStatusBar() ;
  
  resize(800,500) ;
  
}

void MainWindow::createActions()
{
    OpenAct = new QAction(tr("&Close"), this) ;
    OpenAct->setShortcuts(QKeySequence::Close) ;
    OpenAct->setStatusTip(tr("Quit Application")) ;
    connect(OpenAct, SIGNAL(triggered()), this, SLOT(closeapp())) ;
}

// pourquoi en faire une globale, elle n'est utilisée qu'ici
void MainWindow::createMenus()
{
    FileMenu = menuBar()->addMenu(tr("&File"));
    FileMenu->addAction(OpenAct);
    FileMenu->addSeparator();
}

// slot
void MainWindow::onTabChanged(int tabIndex)
{
  if (statusBar()->currentMessage().isEmpty()) setWStatusBar();
}

// ======================================= Pour utilisation à l'extérieur =====================

void MainWindow::createTab(int _idxtTab, QString _TabTitle, QWidget * _metaobject)
{
    _metaobject->setParent(Father);
    Father->insertTab(_idxtTab,_metaobject,_TabTitle);
    setWStatusBar();
    qDebug() << "createTab(): " << "generate class tab : " << _metaobject->metaObject()->className();
}

QLabel * MainWindow::createLabel()
{
    QLabel * label = new QLabel ;
    label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred) ;
    label->setWordWrap(true);
    return label ;
}

void MainWindow::msgBox(QString msg)
{
    QMessageBox msgbox ;
    msgbox.setText(msg) ;
    msgbox.exec();
}

// ====================================== Fonctions de bas niveau

void MainWindow::setWStatusBar()
{
     statusBar()->showMessage(BDD::getdbStatus());
     
     // en cours de test
//     QLabel * logo = new QLabel ;
//     logo->setPixmap(QPixmap(":/resources/logo.jpg")) ;
//     logo->show() ;
//     statusBar()->addPermanentWidget(logo)
}

void MainWindow::closeapp()
{
    QCoreApplication::quit();
}

// inutilisé
void MainWindow::open()
{
    setWStatusBar();
}

//============================================================================ TABGROUP ========

TabGroup::TabGroup()
{
}

TabGroup::~TabGroup()
{
 //  TabList.clear();
}

void TabGroup::addTab(int tabindex, QString tabtitle, Tab * tabclass)
{
    Tabs.append({tabindex,tabtitle,tabclass});
}

void TabGroup::createTabs(MainWindow * win)
{
  qDebug() << "Insert TABS" << BDD::DatabaseConnection;
  for (int i = 0 ; i < Tabs.length() ; i++)
  win->createTab(Tabs[i].tabindex, Tabs[i].tabtitle, Tabs[i].tabclass) ;
}

//========================================================== ONGLETS ============================

Tab::Tab()
{
}

void Tab::initTab(Appli * appli, Shared * params)
{
  initAppli(appli) ;
  if (params) initShared(params, appli) ;
  appli->MyTab = this ;
}

void Tab::initAppli(Appli * appli)
{
  QVBoxLayout * layout = new QVBoxLayout ;
  App = appli ;
  AnswerZone = new QLabel ;
  AnswerZone->setStyleSheet("QLabel {background-color : white;}") ;
  AnswerZone->setAlignment(Qt::AlignCenter) ;
  layout->addWidget(App) ;
  layout->addWidget(AnswerZone) ;
  setLayout(layout) ;
}

void Tab::initShared(Shared * params, Appli * appli)
{
  appli->SharedParams = params ;
}

Tab::~Tab()
{}

void Tab::notImplementedMessage(QString text)
{ answerMessage(text.append(tr(" is not yet implemented"))) ;
}

void Tab::answerMessage(QString text)
{ AnswerZone->setText(text) ;
  AnswerZone->setWordWrap(true) ;
  AnswerZone->repaint() ;
  qDebug() << text ;
}


Shared::Shared()
{}
Shared::~Shared()
{}

Appli::Appli()
{}
Appli::~Appli()
{}

void Appli::addBox(QString title)
{
  
  
}


Box::Box(QString title)
: QGroupBox(title)
{ 
  
}

Box::~Box()
{}

// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#ifndef CORPUSBOXBDD_H
#define CORPUSBOXBDD_H

#include "../BDD/bdd.h"


class CorpusBoxBDD : public BDD
{
    Q_OBJECT
/********************************************************************************************/
public:
      CorpusBoxBDD() ;

      QSqlQueryModel *  sqlLoadCorpus() ;
      QVector<MyMap>    sqlShowCorpus(QString) ;
      int               sqlGetSumCorpus(int) ;
      QString           sqlGetFormats(int) ;
      QString           sqlGetEncoding(int) ;
};
#endif // CORPUSBOXBDD_H

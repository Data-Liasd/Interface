// Copyright 2015-2018 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license

// This box takes a corpora to select from

#include "corpusselect_box.h"

CorpusSelectBox::CorpusSelectBox() 
: Box("Select Corpus")
{
  DbConnection = BDD::DatabaseConnection; // if done in : DbConnection(BDD::DatabaseConnection, would it work or is it too early?

  QGridLayout * corpusselectboxlayout = new QGridLayout ;
  setAlignment(Qt::AlignTop) ;
  setLayout(corpusselectboxlayout);
  corpusselectboxlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));
  corpusselectboxlayout->setAlignment(Qt::AlignTop);

  corpusselectboxlayout->addWidget(new QLabel(tr("Corpus")), 0, 0);
  CorpusValue = new QComboBox;
  loadCorpora();

  CorpusValue->setCurrentIndex(-1);
  connect(CorpusValue, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {setCorpus(index);});
  corpusselectboxlayout->addWidget(CorpusValue, 0, 1);

  //bouton refresh
  QPushButton * refreshcorpora = new QPushButton(tr("Refresh"));
  corpusselectboxlayout->addWidget(refreshcorpora, 0, 3);
  connect(refreshcorpora, &QPushButton::clicked, [=]() {loadCorpora();});
}

QSqlQueryModel * CorpusSelectBox::getCorpusListSqlModel() {
    qDebug() << "getting corpus list from: " << * DbConnection;

    QSqlQueryModel * CorpusListModel = new QSqlQueryModel();
    CorpusListModel->setQuery("SELECT id, nom_corpus FROM corpus", * DbConnection);

    CorpusListModel->setHeaderData(0, Qt::Horizontal, tr("id"));
    CorpusListModel->setHeaderData(1, Qt::Horizontal, tr("nom_corpus"));

    return CorpusListModel;
}

void CorpusSelectBox::loadCorpora()
{
    CorpusValue->setModel(getCorpusListSqlModel());
    CorpusValue->setModelColumn(1);
}

/*
QVector<MyMap> CorpusSelectBox::getCorpusDocumentsList(int corpusId) {
     QVector<MyMap> maplist;
     QSqlQuery req(* DbConnection);

     req.exec(QString("select depot_url_copy, fk_id_corpus from document where fk_id_corpus = %1").arg(corpusId));

     while (req.next())
         maplist.append(MyMap(req.value(0).toString(), req.value(1).toInt()));

     return maplist;

}
*/

void CorpusSelectBox::setCorpus(int index)
{
  CorpusId = 0;
  if (index >= 0)
  {
    QAbstractItemModel * corpusComboBoxModel = CorpusValue->model();
    CorpusId = corpusComboBoxModel->data(corpusComboBoxModel->index(index, 0)).toInt();
  }
  qDebug() << "Corpus est changé pour : " << CorpusId << CorpusValue->itemText(index);
  emit sigCorpusModified(CorpusId);
}

// Copyright 2015-2018 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#ifndef CORPUSSELECTBOX_H
#define CORPUSSELECTBOX_H

#include "../BDD/bdd.h"
#include "../tabdialog.h"

#include <QtWidgets>

// == COMPOSANTS DE LA BOITE
// Cette boite permet de sélectionner parmi un ensemble de corpus
class CorpusSelectBox : public Box
{
    Q_OBJECT

  public:
    
    int CorpusId;
    QSqlDatabase * DbConnection = BDD::DatabaseConnection;
    
    QComboBox * CorpusValue;
    QSqlQueryModel * CorpusListModel;

    // methods
    CorpusSelectBox();

    QSqlQueryModel * getCorpusListSqlModel();
    //QVector<MyMap> getCorpusDocumentsList(int);

    void setCorpus(int);

  signals:
    void sigCorpusModified(int) ;

  public slots:

  protected slots:

  protected:
    void loadCorpora();

};

#endif // CORPUSSELECTBOX_H

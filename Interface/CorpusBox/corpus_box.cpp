// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#include "corpus_box.h"
#include "../tabdialog.h"

QVector<MyMap>  CorpusBox::DocList;
int     CorpusBox::IdCorpus ;

// fonction qui crée les boites pour la sélection du corpus
// CorpusBox, LoadCorpusButton, ConstDate, NumberFiles, CorpusSize, CorpusEncoding, CorpusFormats, CorpusView, CorpusModel
//pour utiliser le corpus box il faut:
//ajouter une nouvelle instance de CorpusBox qui emet un signal corpusshowed avec Id Corpus une fois le corpus est choisi
//capter le signal par un slot dans la classe appelante pour sauvegrader l'idcorpus et faire le traitement voulu

CorpusBox::CorpusBox()
: Box(tr("Corpus selection"))
{
  // la grande boite corpus
  QGridLayout * corpusbiglayout= new QGridLayout ;
  setAlignment(Qt::AlignTop);
  setLayout(corpusbiglayout);

  // la petite boite corpus
  QGroupBox * corpusbox = new QGroupBox(tr("")) ;
  corpusbiglayout->addWidget(corpusbox, 0, 0) ;
  QGridLayout * corpuslayout = new QGridLayout ;
  corpusbox->setAlignment(Qt::AlignTop) ;
  corpusbox->setLayout(corpuslayout) ;

  corpuslayout->addWidget(new QLabel("Corpus"), 0, 0) ;
  corpusboxcmb = sqlGetCorpusBox() ;
  corpuslayout->addWidget(corpusboxcmb, 0, 1) ;
  LoadCorpusButton = new QPushButton("Load") ;     // utile si redevient clickable (si un reset est possible)
  corpuslayout->addWidget(LoadCorpusButton, 0, 2) ;
  connect(LoadCorpusButton, SIGNAL(clicked()), this, SLOT(showCorpus())) ;

  // la grande boite des documents
  QGroupBox * docbox = new QGroupBox(tr("contains")) ;
  corpusbiglayout->addWidget(docbox,1, 0) ;
  QVBoxLayout * doclayout = new QVBoxLayout ;
  docbox->setAlignment(Qt::AlignTop) ;
  docbox->setLayout(doclayout) ;

  // la boite des urls
  createUrlTable() ;
  doclayout->addWidget(CorpusView, 0) ;

  // la boite des descripteurs
  QGroupBox * descriptionbox = new QGroupBox(tr("")) ;
  doclayout->addWidget(descriptionbox, 1) ;
  QGridLayout * descriptionlayout= new QGridLayout ;
  descriptionbox->setLayout(descriptionlayout) ;
  descriptionbox->setAlignment(Qt::AlignTop);

  descriptionlayout->addWidget(new QLabel(tr("Creation date")), 0, 0) ;
  CreationDate = MainWindow::createLabel() ;
  descriptionlayout->addWidget(CreationDate, 0, 1) ;
  descriptionlayout->addWidget(new QLabel(tr("Number of files")), 1, 0) ;
  FileNumber =  MainWindow::createLabel() ;
  descriptionlayout->addWidget(FileNumber, 1, 1) ;
  CorpusSize =  MainWindow::createLabel() ;
  descriptionlayout->addWidget(new QLabel(tr("Corpus size")), 2, 0) ;
  descriptionlayout->addWidget(CorpusSize, 2, 1) ;
  descriptionlayout->addWidget(new QLabel(tr("Charsets")), 3, 0) ;
  CorpusEncoding =  MainWindow::createLabel() ;
  descriptionlayout->addWidget(CorpusEncoding, 4, 0) ;
  descriptionlayout->addWidget(new QLabel(tr("Formats")), 3, 1) ;
  CorpusFormats =  MainWindow::createLabel() ;
  descriptionlayout->addWidget(CorpusFormats, 4, 1) ;
}

QComboBox * CorpusBox::sqlGetCorpusBox()
{
  QComboBox * combobox = new QComboBox();
  combobox->setModel(CorpusBDD->sqlLoadCorpus()) ;
  return combobox;
}

void CorpusBox::createUrlTable()
{ CorpusView = new QTableView ;
  CorpusModel = new QStandardItemModel();
  CorpusView->verticalHeader()->hide();
  CorpusView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  CorpusView->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
  CorpusView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
  CorpusModel->setHorizontalHeaderLabels(QStringList() << tr("Url"));
}

void CorpusBox::showCorpus()
{
  LoadCorpusButton->setEnabled(false) ;
  CorpusModel->setRowCount(0) ;
  CorpusBox::DocList = CorpusBDD->sqlShowCorpus(corpusboxcmb->currentText());
  int i;
  for(i=0;i<CorpusBox::DocList.size();++i)
  {
    QStandardItem * item = new QStandardItem() ;
    item->setText(CorpusBox::DocList.at(i).first) ;
    CorpusModel->appendRow(item) ;
  }
  CorpusView->setModel(CorpusModel) ;
  if (CorpusBox::DocList.size()==0) return ;
  FileNumber->setNum(CorpusBox::DocList.size()) ;
  CorpusBox::IdCorpus = CorpusBox::DocList.at(i-1).second;
  CorpusSize->setText(QString::number((CorpusBDD->sqlGetSumCorpus(CorpusBox::IdCorpus) + 1023) / 1024) + " Ko") ;
  CorpusFormats->setText(sqlGetFormats()) ;
  CorpusEncoding->setText(sqlGetEncoding()) ;
  //emit corpusshowed(CorpusBox::IdCorpus, CorpusBox::DocList);
  qDebug() << "corpus showed is: " << CorpusBox::IdCorpus << " contains " << CorpusBox::DocList.size() << " files";
  MainWindow::msgBox("Corpus is selected, Select the language and preParameters!");
}

QString  CorpusBox::sqlGetFormats()
{
  return CorpusBDD->sqlGetFormats(CorpusBox::IdCorpus);
}

QString CorpusBox::sqlGetEncoding()
{
  return CorpusBDD->sqlGetEncoding(CorpusBox::IdCorpus);
}


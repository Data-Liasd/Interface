// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#include "corpusBDD_box.h"

using namespace std;

CorpusBoxBDD::CorpusBoxBDD()
{
}

/*requetes de CorpusBox*/

QSqlQueryModel * CorpusBoxBDD::sqlLoadCorpus()
{
    qDebug() << "LoadCorpus de BDD: " << BDD::DatabaseConnection;
    QSqlQueryModel * model = new QSqlQueryModel();
    model->setQuery("SELECT nom_corpus FROM corpus",*BDD::DatabaseConnection);
    return model;
}

QVector<MyMap> CorpusBoxBDD::sqlShowCorpus(QString corpusbox)
{
     QVector<MyMap> maplist;
     QSqlQuery req(*BDD::DatabaseConnection);
     req.exec(QString("select depot_url_copy, fk_id_corpus from document where fk_id_corpus = (select id from corpus where nom_corpus = '%1')").arg(corpusbox)) ;
     while (req.next())
         maplist.append(MyMap (req.value(0).toString(),req.value(1).toInt()));
     return maplist;
}

int CorpusBoxBDD::sqlGetSumCorpus(int idcorpus)
{
    QSqlQuery req(*BDD::DatabaseConnection) ;
    req.exec(QString("select SUM(size) from document where fk_id_corpus = %1").arg(idcorpus)) ;
    if (req.next()) return req.value(0).toInt() ;
    return 0 ;
}

QString  CorpusBoxBDD::sqlGetFormats(int idcorpus)
{
    QString listformats ;
    QSqlQuery req(*BDD::DatabaseConnection) ;
    req.exec(QString("(select distinct extension from format, document where fk_id_corpus = %1 and format.id = document.fk_id_format)").arg(idcorpus));
    while (req.next()) listformats.append(req.value(0).toString() +", ") ;
    return listformats ;
}

QString CorpusBoxBDD::sqlGetEncoding(int idcorpus)
{
    QString listencoding ;
    QSqlQuery req(*BDD::DatabaseConnection) ;
    req.prepare("(select distinct designation_codage from codage, document where fk_id_corpus=:idcorpus and   codage.id = document.fk_id_codage)");
    req.bindValue(":idcorpus", idcorpus) ;
    req.exec() ;
    while (req.next()) listencoding.append(req.value(0).toString() +", ") ;
    return listencoding ;
}

// Copyright 2015-2017 Adelle Abdallah, Gilles Bernard, Otman Manad
// This program is licensed under the terms of the GNU GPLv3 license
#ifndef CORPUSBOX_H
#define CORPUSBOX_H
#include "corpusBDD_box.h"
#include "../tabdialog.h"
#include "QtWidgets"

//================================ COMPOSANTS DE LA BOITE

class CorpusBox : public Box
{
    Q_OBJECT

public:
    CorpusBox() ;

    CorpusBoxBDD *        CorpusBDD ;
    static QVector<MyMap> DocList ;
    static int            IdCorpus ;

protected slots:
    void showCorpus() ;

protected:

    void                  createUrlTable() ;
    QComboBox *           corpusboxcmb ;
    QPushButton *         LoadCorpusButton ;
    QComboBox *           sqlGetCorpusBox() ;
    
    QString               sqlGetFormats() ;
    QString               sqlGetEncoding() ;

    QStandardItemModel *  CorpusModel ;
    QTableView *          CorpusView ;
    QLabel *              CreationDate ;
    QLabel *              FileNumber ;
    QLabel *              CorpusSize ;
    QLabel *              CorpusEncoding ;
    QLabel *              CorpusFormats ;
} ;

#endif // CORPUSBOX_H

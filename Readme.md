This is the Interface library for the works of the group Data of Liasd Laboratory

Authors : Adelle Abdallah, Gilles Bernard, Otman Manad

Copyright 2015-2019

GNU GPLv3 License

Version 1.0.3

This module brings common methods and objects for
- interfacing with database
- interfacing with filesystem and internet to build a corpus
- grouping tabs for different steps of an application
- keep a common look and feel for the group works

A short user documentation is available.

Some slight inconsistencies have been removed since Version 1.0.3 but no important changes

Next version (1.1) will add:
- signals for managing languages in corpusselect_box
- our own functionalities for printing and loading complex jsonfiles representing tries as existing libraries do not support very nested trees nor very big files

It should be out by july 2019.